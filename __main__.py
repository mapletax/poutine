#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incomedeclaration@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging
import sys
import os
import tempfile
import io

from ..timber.indented import monkeypatch_stack_indenting_formatter


logger = logging.getLogger(__name__)


def main(argv=None):
	import argparse
	parser = argparse.ArgumentParser(
	 description="Income declaration forms tool",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "federal",
	 help="Parse a federal form",
	)
	subp.add_argument("dst",
	)
	subp.add_argument("src",
	)
	subp.add_argument("kind",
	)
	subp.add_argument(
	 "year",
	 type=int,
	)
	subp.add_argument("--print-aliases",
	 type=int,
	 choices=(0,1),
	 default=0,
	 help="to make things more complicated, federal forms identify fields" \
	  " using two numeric identifiers",
	)

	def doit(args):
		from ..formfiller.serialization import save
		from .federal import process_federal_form
		logger.info("Processing %s", args.src)
		field_meta, aliases = process_federal_form(args.src, args.kind, args.year)
		save(field_meta, args.dst)
		if args.print_aliases:
			for k,v in sorted(aliases.items(), key=lambda x: int(x[0])):
				print(f" {k}: {v},")

	subp.set_defaults(func=doit)

	subp = subparsers.add_parser(
	 "qc",
	 help="Parse a qc form",
	)
	subp.add_argument("dst",
	)
	subp.add_argument("src",
	)
	subp.add_argument("kind",
	)
	subp.add_argument(
	 "year",
	 type=int,
	)

	def doit(args):
		from ..formfiller.serialization import save
		from .qc import process_qc_form
		logger.info("Processing %s", args.src)
		field_meta = process_qc_form(args.src, args.kind, args.year)
		save(field_meta, args.dst)

	subp.set_defaults(func=doit)


	args = parser.parse_args(argv)


	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		 logger=logging.root,
		 isatty=True,
		)
	except ImportError:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	# Silence annoying messages
	for annoying_logger in (
	  "pdfminer.psparser",
	  "pdfminer.pdfparser",
	  "pdfminer.pdfdocument",
	 ):
		logging.getLogger(annoying_logger).setLevel(logging.INFO)

	with monkeypatch_stack_indenting_formatter():

		func = getattr(args, 'func', None)
		if func is not None:
			args.func(args)
		else:
			parser.print_help()
			return 1


if __name__ == "__main__":
	raise SystemExit(main())
