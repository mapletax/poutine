#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re
import logging

from ..formparse.guesswork import extract_fields


logger = logging.getLogger(__name__)


identification_map_tp1_22 = {
 "char-1": "last-name",
 "char-2": "first-name",
 "checkbox-3": "first-declaration",
 "checkbox-4": "sex-male",
 "checkbox-4/1": "sex-female",
 "checkbox-5": "lang-fr",
 "checkbox-5/1": "lang-en",
 "char-6": "dob",
 "char-7": "mailing/appartment-number",
 "char-7/1": "mailing/street-number",
 "char-7/2": "mailing/street-name-po-box",
 "char-8": "mailing/city",
 "char-8/1": "mailing/province",
 "char-9": "mailing/postal-code",
 "char-10": "phone-number",
 "char-10.1": "email",
 "checkbox-10.2": "email-only",
 "char-11": "sin",
 "checkbox-12": "not-spouse",
 "checkbox-12/1": "yes-spouse",
 "char-13": "situation-change-date",
 "char-17": "residence-if-not-qc",
 "char-18": "residence-landing-date",
 "char-19": "residence-departure-date",
 "numeric-19": "foreign-income",
 "char-20": "dod",
 "char-21": "bankruptcy-date",
 "checkbox-21": "bankruptcy-after",
 "checkbox-21/1": "bankruptcy-before",
 "checkbox-13": "bankruptcy-rrq-choice",
 "checkbox-23": "death-distinct",
 "checkbox-24": "cryptobro",
 "char-31": "spouse/last-name",
 "char-32": "spouse/first-name",
 "char-36": "spouse/dob",
 "char-37": "spouse/dod",
 "char-41": "spouse/sin",
 "checkbox-50": "spouse/self-employed",
 "numeric-51": "spouse/net-revenue",
 "char-52": "spouse/residence-if-not-qc",
 "checkbox-94": "job-outside-canada",
 "checkbox-95": "job-outside-quebec",
 "char-498": "phone-number",
 "char-499": "phone-number-work",
}

identification_map_tp1_23 = identification_map_tp1_22


def process_qc_form(path, kind, year):
	out = extract_fields(path)

	for page, data in out.items():
		for field_name, field_data in data["fields"].items():
			field_data["font-color"] = [0, 0, 1, 1] # blue

	if kind == "TP-1.D":
		for page, data in out.items():
			for fqn, field_data in data["fields"].items():
				field = out[page]["fields"][fqn]
				aliases = dict()
				if year == 2022:
					identity = identification_map_tp1_22.get(fqn)
				elif year == 2023:
					identity = identification_map_tp1_23.get(fqn)
				else:
					raise NotImplementedError(year)
				if identity is not None:
					aliases["identity"] = identity
					field["aliases"] = aliases
					faliases = field.setdefault("aliases", {})
					faliases |= aliases
					logger.debug("Assigning alias {%s} to field {%s}",
					 identity,
					 fqn,
					)
	else: # schedules

		# Workaround numeric field look
		for page, data in out.items():
			for fqn, field_data in data["fields"].items():
				field = out[page]["fields"][fqn]
				if field.get("align") == "decimal":
					field["font-size"] = 9
					field["character-spacing"] = 12

	return out
