###########################################
Income Declarations - Form Recognition Code
###########################################


Usage
#####

TODO


Design
######


Canada Revenue Agency
*********************

- The CRA forms have version containing PDF "AcroForm" forms,
  which help with field identification.

Revenu Québec
*************

- They don't have fillable PDFs so we need to do recognition
  on a regular PDF.

- For their numeric fields, we rely on presence of a comma,
  on the right of a numeric field identifier.

