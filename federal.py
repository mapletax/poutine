#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-mapletax-poutine@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re
import copy
import logging

from ..formparse.acroform import extract_fields


logger = logging.getLogger(__name__)


pattern_stable_line = re.compile(r"^(Line|Ligne) (?P<stable>\d+)\. (Line|Ligne) (?P<line>\d+)\.")
pattern_line_stable = re.compile(r"^(Line|Ligne) (?P<line>\d+)\. (Amount (on|from) line|Montant de la ligne) (?P<stable>\d+)\.")
pattern_line = re.compile(r"(^|.*\. )(Line|Ligne) (?P<line>\d+)\.")
pattern_line2 = re.compile(r"^(Amount from line|Montant de la ligne) (?P<line>\d+)\.")
pattern_part_line = re.compile(r"Part(|ie) (?P<part>\d+)\. (Line|Ligne) (?P<line>\d+)\.")
pattern_spacing = re.compile(r".* (?P<length>\d+) (characters|digits|caractères|chiffres)\.")


"""
Maps are references to user identity data(see sample at the mapletax/toonies/samples/...)
"""

identification_map_5005r_23 = {
 "ID_FirstNameInitial[0]": "first-name",
 "ID_LastName[0]":         "last-name",
 "DateBirth_Comb[0]":      "dob",
 "DateDeath_Comb[0]":      "dod",
 "ID_MailingAddress[0]": "mailing/address",
 "PostalCode[0]": "mailing/postal-code",
 "ID_POBox[0]": "mailing/po-box",
 "ID_RuralRoute[0]": "mailing/rural-route",
 "ID_City[0]": "mailing/city",
 "EmailAddress[0]":        "e-mail",
 "Prov_DropDown-Residence[0]": "residence/province",
 "Residence_Info[0]/Prov_DropDown[0]": "residence/province-current",
 "Prov_DropDown[0]": "mailing/province",
 "MaritalStatus[0]":       "married",
 "MaritalStatus[1]":       "common-law",
 "MaritalStatus[2]":       "widowed",
 "MaritalStatus[3]":       "divorced",
 "MaritalStatus[4]":       "separated",
 "MaritalStatus[5]":       "single",
 "Telephone[0]": "phone-number",
 "Spouse_First_Name[0]": "spouse/first-name",
 "Checkbox[0]": "spouse/self-employed",
 "Date_Entry[0]/DateMMDD_Comb_BordersAll_Std[0]/DateMMDD_Comb[0]": "date-of-entry",
 "Date_Departure[0]/DateMMDD_Comb_BordersAll_Std[0]/DateMMDD_Comb[0]": "date-of-departure",
 "Info_Spouse_CLP[0]/SIN_Comb_BordersAll[0]/SIN_Comb[0]": "spouse/sin",
 "Option1[0]/A_CheckBox[0]": "elections/citizenship",
 "Option2[0]/A_CheckBox[0]": "elections/no-citizenship",
 "Option1[0]/B_Authorize_CheckBox[0]": "elections/auth-for-election",
 "Option2[0]/B_Authorize_CheckBox[0]": "elections/no-auth-for-election",
 "Tax_exempt[0]/Exempt[0]/Spouse_SelfEmployed[0]": "indian-act-income",
 "Option1[0]/ForeignProperty_CheckBox[0]": "foreign-property",
 "Option2[0]/ForeignProperty_CheckBox[0]": "no-foreign-property",
 "SIN_Comb[0]":            "sin",
}

identification_map_5005r_22 = {
 "ID_FirstNameInitial[0]": "first-name",
 "ID_LastName[0]":         "last-name",
 "DateBirth_Comb[0]":      "dob",
 "DateDeath_Comb[0]":      "dod",
 "ID_MailingAddress[0]": "mailing/address",
 "mailingAddress[0]": "mailing/mailing-address",
 "PostalCode[0]": "mailing/postal-code",
 "ID_POBox[0]": "mailing/po-box",
 "ID_RuralRoute[0]": "mailing/rural-route",
 "ID_City[0]": "mailing/city",
 "EmailAddress[0]":        "e-mail",
 "Prov_DropDown-Residence[0]": "residence/province",
 "Residence_Info[0]/Prov_DropDown[0]": "residence/province-current",
 "Prov_DropDown[0]": "mailing/province",
 "MaritalStatus[0]":       "married",
 "MaritalStatus[1]":       "common-law",
 "MaritalStatus[2]":       "widowed",
 "MaritalStatus[3]":       "divorced",
 "MaritalStatus[4]":       "separated",
 "MaritalStatus[5]":       "single",
 "Telephone[0]": "phone-number",
 "Spouse_First_Name[0]": "spouse/first-name",
 "Checkbox[0]": "spouse/self-employed",
 "Date_Entry[0]/DateMMDD_Comb_BordersAll_Std[0]/DateMMDD_Comb[0]": "date-of-entry",
 "Date_Departure[0]/DateMMDD_Comb_BordersAll_Std[0]/DateMMDD_Comb[0]": "date-of-departure",
 "Info_Spouse_CLP[0]/SIN_Comb_BordersAll[0]/SIN_Comb[0]": "spouse/sin",
 "A_CheckBox[0]": "elections/citizenship",
 "A_CheckBox[1]": "elections/no-citizenship",
 "B_Authorize_CheckBox[0]": "elections/auth-for-election",
 "B_Authorize_CheckBox[1]": "elections/no-auth-for-election",
 "Tax_exempt[0]/Q[0]/Spouse_SelfEmployed[0]": "indian-act-income",
 "ForeignProperty_CheckBox[0]": "foreign-property",
 "ForeignProperty_CheckBox[1]": "no-foreign-property",
 "SIN_Comb[0]":            "sin",
}


def process_federal_form(path, kind, year) -> tuple[dict, dict]:
	out = extract_fields(path)
	res = copy.deepcopy(out)
	aliases_storage = {}

	for page, data in out.items():
		for field_name, field_data in data["fields"].items():
			field = res[page]["fields"][field_name]
			if "value" in field:
				del field["value"]

			field["align"] = "center"
			field["align-v"] = "center"

	if kind == "5005-r":
		for page, data in out.items():
			for field_name, field_data in data["fields"].items():
				field = res[page]["fields"][field_name]
				aliases = {}

				desc = field_data["description"]
				if (m := pattern_stable_line.match(desc)) is not None:
					line = m.group("line")
					stable = m.group("stable")
					assert len(stable) == 5, f"{stable}"
				elif (m := pattern_line_stable.match(desc)) is not None:
					line = m.group("line")
					stable = m.group("stable")
					assert len(stable) == 5, f"line={line} stable={stable} {desc}"
				elif (m := pattern_line2.match(desc)) is not None:
					line = m.group("line")
					stable = None
					if len(line) == 5:
						line, stable = stable, line
				elif (m := pattern_line.match(desc)) is not None:
					line = m.group("line")
					stable = None
					if len(line) == 5:
						line, stable = stable, line
				else:
					line = None
					stable = None

				if line:
					aliases["line"] = int(line)

				if stable:
					aliases["stable"] = int(stable)

				if aliases: # ~ line or stable
					faliases = field.setdefault("aliases", {})
					faliases |= aliases
					if field.get("class") != "checkbox":
						# We also restyle these numeric fields
						l, t, w, h = field["bounding-box"]
						field["origin"] = round(l + w - 18, 5), round(t + h - 2, 5)
						field["align"] = "decimal"
						field["format"] = ".2f"
						field["max-length"] = 13
						field["font-size"] = 9

				if line and stable:
					aliases_storage[line] = stable

	if kind.startswith(("5005-s", "5000-s")):
		for page, data in out.items():
			for field_name, field_data in data["fields"].items():
				field = res[page]["fields"][field_name]
				aliases = {}

				desc = field_data["description"]
				if (m := pattern_stable_line.match(desc)) is not None:
					line = m.group("line")
					stable = m.group("stable")
				elif (m := pattern_line_stable.match(desc)) is not None:
					line = m.group("line")
					stable = m.group("stable")
				elif (m := pattern_line2.match(desc)) is not None:
					line = m.group("line")
					stable = None
				elif (m := pattern_part_line.match(desc)) is not None:
					# Multi-part schedule : if 5 digits, it's stable across the form
					# otherwise prefix with part identifier, and for convenience,
					# use a float
					line = m.group("line")
					if len(line) != 5:
						line = int(m.group("part")) + int(m.group("line")) / 100
					stable = None
				elif (m := pattern_line.match(desc)) is not None:
					line = m.group("line")
					stable = None
				else:
					line = None
					stable = None

				if line is not None:
					if isinstance(line, str):
						if len(line) == 5:
							aliases["stable"] = int(line)
						else:
							aliases["line"] = int(line)
					else:
						aliases["line"] = line

				if stable is not None:
					if len(stable) == 5:
						aliases["stable"] = int(stable)
					else:
						aliases["ref"] = int(stable)

				if aliases: # ~ line or stable
					faliases = field.setdefault("aliases", {})
					faliases |= aliases

					# We also restyle these numeric fields
					l, t, w, h = field["bounding-box"]
					field["origin"] = round(l + w - 18, 5), round(t + h - 2, 5)
					field["align"] = "decimal"
					field["format"] = ".2f"
					field["max-length"] = 13
					field["font-size"] = 9

				if line and stable:
					aliases_storage[line] = stable

	if kind == "5005-r":
		for page, data in out.items():
			for fqn, field_data in data["fields"].items():
				field = res[page]["fields"][fqn]
				field_name = fqn.split("/")[-1]

				if year == 2022:
					idmap = identification_map_5005r_22
				elif year == 2023:
					idmap = identification_map_5005r_23
				else:
					raise NotImplementedError(year)

				for mapk, mapv in idmap.items():
					if fqn.endswith(mapk):
						identity = mapv
						break
				else:
					continue

				aliases["identity"] = identity
				faliases = field.setdefault("aliases", {})
				faliases |= aliases
				logger.debug("Assigning identification alias {%s} to field {%s}",
				 identity,
				 fqn,
				)

			# Styling of some identity fields
			for fqn, field_data in data["fields"].items():
				field = res[page]["fields"][fqn]
				field_name = fqn.split("/")[-1]

				m = pattern_spacing.match(field_data["description"])
				if m is not None:
					length = int(m.group("length"))
					fqn_l = fqn.lower()
					if ("entry" in fqn_l) or ("departure" in fqn_l):
						length = 4
					elif "date" in field_name.lower():
						length = 8
					l, t, w, h = field["bounding-box"]
					char_box_length = w / length
					field["max-length"] = length
					field["origin"] = round(l + char_box_length/2, 5), round(t + h - 3, 5)
					field["character-spacing"] = char_box_length
					field["align"] = "left"

	return res, aliases_storage
